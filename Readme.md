[![](http://www.r-pkg.org/badges/version/moveWindSpeed)](http://cran.rstudio.com/web/packages/moveWindSpeed/index.html)
[![](http://cranlogs.r-pkg.org/badges/moveWindSpeed)](http://cran.rstudio.com/web/packages/moveWindSpeed/index.html)
[![build status](https://gitlab.com/bartk/moveWindSpeed/badges/master/build.svg)](https://gitlab.com/bartk/moveWindSpeed/builds)

### Install the development version
``` R
require('devtools')
install_git('https://gitlab.com/bartk/moveWindSpeed.git')
```
